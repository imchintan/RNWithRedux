/**
 * Navigation Routes defind
 */
import { StackNavigator } from 'react-navigation';
import Screen1 from '@containers/Screen1';

const routes = {
    Screen1: {
        screen: Screen1,
    },
};

const routeConfig = {
    initialRouteName: 'Screen1',
    headerMode: 'none',
    navigationOptions: {
     gesturesEnabled: false,
   }
};

const AppNavigation = StackNavigator(routes,routeConfig);

export default AppNavigation;
