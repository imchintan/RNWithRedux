import Container from './Container';
import Header from './Header';
import Content from './Content';
import Footer from './Footer';
import Button from './Button';
import FAB from './FAB';
import Loader from './Loader';
import Toast from './Toast';
import TabBarBottom from './TabBarBottom';
import Icon from 'react-native-fa-icons';

module.exports = {
    Container,
    ...Header,
    Content,
    ...Footer,
    Button,
    FAB,
    Loader,
    Toast,
    TabBarBottom,
    Icon,
}
